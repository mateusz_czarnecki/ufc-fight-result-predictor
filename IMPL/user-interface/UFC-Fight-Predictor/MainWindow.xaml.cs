﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Forms;

namespace UFC_Fight_Predictor
{
    public partial class MainWindow : Window
    {
        private string trainDatasetPath;
        private string testDatasetPath;
        private string modelPath;

        public MainWindow()
        {
            InitializeComponent();

        }

        private void OnClickOpenTrainingDataset(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv|XML files (*.xml)|*.xml";

            if (openFileDialog.ShowDialog() == true)
            {
                trainDatasetPath = openFileDialog.FileName;
            }
        }

        private void OnClickOpenTestingDataset(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv|XML files (*.xml)|*.xml";

            if (openFileDialog.ShowDialog() == true)
            {
                testDatasetPath = openFileDialog.FileName;
            }
        }

        private void OnClickOpenModel(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            modelPath = folderBrowserDialog.SelectedPath;
        }

        private void OnClickTrain(object sender, RoutedEventArgs e)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Users\xmitt\AppData\Local\Programs\Python\Python38\python.exe";

            var script = @"V:\Repositories\ufc-fight-result-predictor\neural-network\train.py";
            var model = @"V:\Repositories\ufc-fight-result-predictor\models\" + ModelName.Text;

            psi.Arguments = $"\"{script}\" \"{trainDatasetPath}\" \"{model}\"";

            psi.UseShellExecute = false;
            psi.CreateNoWindow = false;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = false;

            var results = "";

            using (var process = Process.Start(psi))
            {
                results = process.StandardOutput.ReadToEnd();
            }
        }

        private void OnClickPredict(object sender, RoutedEventArgs e)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Users\xmitt\AppData\Local\Programs\Python\Python38\python.exe";

            var script = @"V:\Repositories\ufc-fight-result-predictor\neural-network\test.py";
            psi.Arguments = $"\"{script}\" \"{modelPath}\" \"{testDatasetPath}\"";

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            var results = "";

            using (var process = Process.Start(psi))
            {
                results = process.StandardOutput.ReadToEnd();
            }

            int index = 0;
            results = results.Replace(System.Environment.NewLine, string.Empty);
            string[] resultArray = results.Split(' ');

            foreach (string element in resultArray)
            {
                if (element == "Total:")
                {
                    TotalNumberOfTestedFights.Text = resultArray[index + 1];
                    NumberOfCorrectPredictions.Text = resultArray[index + 3];
                    NumberOfWrongPredictions.Text = resultArray[index + 5];
                    PercentageEffectiveness.Text = resultArray[index + 8];
                }
                index++;
            }
        }
    }
}
