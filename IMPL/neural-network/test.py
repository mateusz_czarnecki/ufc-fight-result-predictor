import tensorflow as tf
import pandas as pd
# import sklearn as sklearn
import sys

model = tf.keras.models.load_model(sys.argv[1])

test_data_path = sys.argv[2]

test_data = pd.read_csv(test_data_path)

test_data['Winner'] = pd.Categorical(test_data['Winner'])
test_data['Winner'] = test_data.Winner.cat.codes

test_data['title_bout'] = pd.Categorical(test_data['title_bout'])
test_data['title_bout'] = test_data.title_bout.cat.codes

# umieszczenie wynikow zbiorow w zmiennych target
test_target = test_data.pop('Winner')

# przygotowanie zbioru danych
test_dataset = tf.data.Dataset.from_tensor_slices(test_data.values)

test_dataset = test_dataset.batch(1)

# przewidywanie
prediction_result = model.predict(test_dataset)

prediction_result = [1 if y >= 0.5 else 0 for y in prediction_result]

total = 0
correct = 0
wrong = 0

for element in test_target:
    total = total + 1
    if(element == prediction_result[total-1]):
        correct = correct + 1
    else:
        wrong = wrong + 1

print("==Results== ")
print("Total: " + str(total) + " ")
print("Correct: " + str(correct) + " ")
print("Wrong: " + str(wrong) + " ")
print("Correct percent: " + str("{:.2f}".format(correct/total * 100)) + "%")
