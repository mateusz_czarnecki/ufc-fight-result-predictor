import tensorflow as tf
import pandas as pd
import sklearn as sklearn
import numpy as np
import sys
import matplotlib.pyplot as plot

train_data_path = sys.argv[1]
train_data = pd.read_csv(train_data_path)

test_data_path = sys.argv[3]
test_data = pd.read_csv(test_data_path)

# zmiana wartosc kolumny Winner (red/blue) na wartosci dyskretne 1/0
train_data['Winner'] = pd.Categorical(train_data['Winner'])
train_data['Winner'] = train_data.Winner.cat.codes

test_data['Winner'] = pd.Categorical(test_data['Winner'])
test_data['Winner'] = test_data.Winner.cat.codes

# zmiana wartosci kolumny title_bout (true/false) na wartosci dyskretne 1/0
train_data['title_bout'] = pd.Categorical(train_data['title_bout'])
train_data['title_bout'] = train_data.title_bout.cat.codes

test_data['title_bout'] = pd.Categorical(test_data['title_bout'])
test_data['title_bout'] = test_data.title_bout.cat.codes

# umieszczenie wynikow zbiorow w zmiennych target
train_target = train_data.pop('Winner')
test_target = test_data.pop('Winner')

# przygotowanie zbioru danych
train_dataset = tf.data.Dataset.from_tensor_slices((train_data.values, train_target.values))
test_dataset = tf.data.Dataset.from_tensor_slices((test_data.values, test_target.values))

train_dataset = train_dataset.batch(1)
test_dataset = test_dataset.batch(1)

# Optymalizator
Optimiser=tf.keras.optimizers.SGD(learning_rate=0.00006,momentum=0.7)

# funkcja tworzaca model
def get_compiled_model():
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Dense(62,activation='elu'))

    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))
    model.add(tf.keras.layers.Dense(62,activation='elu'))

    model.add(tf.keras.layers.Dense(1,activation="sigmoid"))
    model.compile(loss="binary_crossentropy",
        optimizer=Optimiser,
        metrics=["accuracy"])
    return model

# utworzenie skompilowanego modelu
model = get_compiled_model()

# trenowanie modelu
epochs_count = 4000
result = model.fit(train_dataset, epochs=epochs_count, validation_data=test_dataset)

#wyniki danych treningowych
acc = result.history['accuracy']
loss = result.history['loss']

#wyniki danych testowych
val_acc = result.history['val_accuracy']
val_loss = result.history['val_loss']

plot.figure(figsize=(8, 8))
plot.subplot(1, 2, 1)
plot.plot(range(epochs_count), acc, label='Training Accuracy')
plot.plot(range(epochs_count), val_acc, label='Test Accuracy')
plot.legend(loc='lower right')
plot.title('Training and Test Accuracy')

plot.subplot(1, 2, 2)
plot.plot(range(epochs_count), loss, label='Training Loss')
plot.plot(range(epochs_count), val_loss, label='Test Loss')
plot.legend(loc='upper right')
plot.title('Training and Test Loss')
plot.show()

model.save(sys.argv[2])
